from rest_framework import serializers
from djangoapp.models import employees

class employeesSerializer(serializers.ModelSerializer):
    class Meta:
        model= employees
        fields=('firstname','lastname')
        #fields= '__all__'

